#ifndef STREAM9_LINUX_TIME_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_TIME_TEST_SRC_NAMESPACE_HPP

namespace stream9 {}
namespace stream9::linux {}
namespace stream9::json {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace lx { using namespace st9::linux; }
namespace json { using namespace st9::json; }

} // namespace testing

#endif // STREAM9_LINUX_TIME_TEST_SRC_NAMESPACE_HPP
