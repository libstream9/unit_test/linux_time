#include <stream9/linux/time/string.hpp>

#include "namespace.hpp"

#include <stream9/linux/time/time.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(to_iso8601_duration_)

    BOOST_AUTO_TEST_CASE(from_time_t_1_)
    {
        ::time_t t = 60;

        auto s = lx::to_iso8601_duration(t);

        BOOST_TEST(s == "PT1M");
    }

    BOOST_AUTO_TEST_CASE(from_time_t_2_)
    {
        ::time_t t = 0;

        auto s = lx::to_iso8601_duration(t);

        BOOST_TEST(s == "PT0S");
    }

    BOOST_AUTO_TEST_CASE(timespec_1_)
    {
        struct ::timespec t {
            .tv_sec = 1,
            .tv_nsec = 1
        };

        auto s = lx::to_iso8601_duration(t);

        BOOST_TEST(s == "PT1.000000001S");
    }

    BOOST_AUTO_TEST_CASE(timespec_2_)
    {
        struct ::timespec t {
            .tv_sec = 1,
            .tv_nsec = 10
        };

        auto s = lx::to_iso8601_duration(t);

        BOOST_TEST(s == "PT1.00000001S");
    }

    BOOST_AUTO_TEST_CASE(timespec_3_)
    {
        struct ::timespec t {
            .tv_sec = 0,
            .tv_nsec = 999999999,
        };

        auto s = lx::to_iso8601_duration(t);

        BOOST_TEST(s == "PT0.999999999S");
    }

    BOOST_AUTO_TEST_CASE(tm_1_)
    {
        try {
            struct ::tm t {};
            t.tm_mday = 1;

            auto s = lx::to_iso8601_duration(t);

            BOOST_TEST(s == "PT0S");
        }
        catch (...) {
            st9::print_error();
        }
    }

    BOOST_AUTO_TEST_CASE(tm_2_)
    {
        try {
            struct ::tm t {};
            t.tm_sec = 1;
            t.tm_min = 2;
            t.tm_hour = 3;
            t.tm_mday = 4;
            t.tm_mon = 5;
            t.tm_year = 6;

            auto s = lx::to_iso8601_duration(t);

            BOOST_TEST(s == "P6Y5M3DT3H2M1S");
        }
        catch (...) {
            st9::print_error();
        }
    }

BOOST_AUTO_TEST_SUITE_END() // to_iso8601_duration_

BOOST_AUTO_TEST_SUITE(to_iso8601_datetime_)

    BOOST_AUTO_TEST_CASE(time_t_)
    {
        ::time_t t = 0;

        auto s = lx::to_iso8601_datetime(t);

        BOOST_TEST(s == "1970-01-01T09:00:00+0900");
    }

    BOOST_AUTO_TEST_CASE(timespec_)
    {
        struct ::timespec t { .tv_sec = 0, .tv_nsec = 1 };

        auto s = lx::to_iso8601_datetime(t);

        BOOST_TEST(s == "1970-01-01T09:00:00+0900");
    }

BOOST_AUTO_TEST_SUITE_END() // to_iso8601_datetime_

} // namespace testing
