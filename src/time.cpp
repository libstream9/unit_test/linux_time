#include <stream9/linux/time/time.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(time_)

    BOOST_AUTO_TEST_CASE(ok_)
    {
        auto t = lx::time();

        BOOST_TEST(t != 0);
    }

BOOST_AUTO_TEST_SUITE_END() // time_

BOOST_AUTO_TEST_SUITE(clock_gettime_)

    BOOST_AUTO_TEST_CASE(ok_)
    {
        auto ts = lx::clock_gettime(CLOCK_REALTIME);

        BOOST_TEST(ts.tv_sec != 0);
        BOOST_TEST(ts.tv_nsec != 0);
    }

BOOST_AUTO_TEST_SUITE_END() // clock_gettime_

} // namespace testing
