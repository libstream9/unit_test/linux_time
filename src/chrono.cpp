#include <stream9/linux/time/chrono.hpp>

#include "namespace.hpp"

#include <stream9/linux/time/json.hpp>

#include <chrono>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(chrono_)

    namespace C = std::chrono;

    BOOST_AUTO_TEST_CASE(duration_to_timespec_1_)
    {
        C::seconds d { 0 };

        auto ts = lx::to_timespec(d);

        json::object expected {
            { "tv_sec", 0, },
            { "tv_nsec", 0 },
        };

        BOOST_TEST(json::value_from(ts) == expected);
    }

    BOOST_AUTO_TEST_CASE(duration_to_timespec_2_)
    {
        C::milliseconds d { 1234 };

        auto ts = lx::to_timespec(d);

        json::object expected {
            { "tv_sec", 1, },
            { "tv_nsec", 234000000 },
        };

        BOOST_TEST(json::value_from(ts) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // chrono_

} // namespace testing
