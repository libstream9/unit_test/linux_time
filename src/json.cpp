#include <stream9/linux/time/json.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace json { using namespace stream9::json; }

BOOST_AUTO_TEST_SUITE(json_)

    BOOST_AUTO_TEST_CASE(struct_timespec_)
    {
        struct ::timespec ts {};

        auto v = json::value_from(ts);

        json::object expected {
            { "tv_sec", 0 },
            { "tv_nsec", 0 },
        };

        BOOST_TEST(v == expected);
    }

    BOOST_AUTO_TEST_CASE(struct_tm_)
    {
        struct ::tm t {};

        auto v = json::value_from(t);

        json::object expected {
            { "tm_sec", 0 },
            { "tm_min", 0 },
            { "tm_hour", 0 },
            { "tm_mday", 0 },
            { "tm_mon", 0 },
            { "tm_year", 0 },
            { "tm_wday", 0 },
            { "tm_yday", 0 },
            { "tm_isdst", 0 },
        };

        BOOST_TEST(v == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // json_

} // namespace testing
